<!-- CONTAINER -->
<?php
	$llorix_one_lite_header_logo = get_theme_mod('llorix_one_lite_header_logo', llorix_one_lite_get_file('/images/logo-2.png'));
	$llorix_one_lite_header_title = get_theme_mod('llorix_one_lite_header_title',esc_html__('Simple, Reliable and Awesome.','llorix-one-lite'));
	$llorix_one_lite_header_subtitle = get_theme_mod('llorix_one_lite_header_subtitle','Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
	$llorix_one_lite_header_button_text = get_theme_mod('llorix_one_lite_header_button_text',esc_html__('GET STARTED','llorix-one-lite'));
	$llorix_one_lite_header_button_link = get_theme_mod('llorix_one_lite_header_button_link','#');
	$llorix_one_lite_enable_move = get_theme_mod('llorix_one_lite_enable_move');
	$llorix_one_lite_first_layer = get_theme_mod('llorix_one_lite_first_layer', llorix_one_lite_get_file('/images/background1.png'));
	$llorix_one_lite_second_layer = get_theme_mod('llorix_one_lite_second_layer',llorix_one_lite_get_file('/images/background2.png'));

?>

<?php
	if( !empty($llorix_one_lite_enable_move) && $llorix_one_lite_enable_move ) {
		
		echo '<ul id="llorix_one_lite_move">';


			if ( empty($llorix_one_lite_first_layer) && empty($llorix_one_lite_second_layer) ) {

				$llorix_one_header_image2 = get_header_image();
				echo '<li class="layer layer1" data-depth="0.10" style="background-image: url('.$llorix_one_header_image2.');"></li>';

			} else {

				if( !empty($llorix_one_lite_first_layer) )  {
					echo '<li class="layer layer1" data-depth="0.10" style="background-image: url('.$llorix_one_lite_first_layer.');"></li>';
				}
				if( !empty($llorix_one_lite_second_layer) ) {
					echo '<li class="layer layer2" data-depth="0.20" style="background-image: url('.$llorix_one_lite_second_layer.');"></li>';
				}

			}

		echo '</ul>';

	}
?>

		<div class="overlay-layer-wrap">
			<div class="container overlay-layer" id="llorix_one_lite_header">

			<!-- ONLY LOGO ON HEADER -->
			<?php
				if( !empty($llorix_one_lite_header_logo) ){
					echo '<div class="only-logo"><div id="only-logo-inner" class="navbar"><div id="llorix_one_lite_only_logo" class="navbar-header"><img src="'.esc_url($llorix_one_lite_header_logo).'"   alt=""></div></div></div>';
				} elseif ( isset( $wp_customize )   ) {
					echo '<div class="only-logo"><div id="only-logo-inner" class="navbar"><div id="llorix_one_lite_only_logo" class="navbar-header"><img src="" alt=""></div></div></div>';
				}
			?>
			<!-- /END ONLY LOGO ON HEADER -->

			<div class="row">
				<div class="col-md-12 intro-section-text-wrap">

					<!-- HEADING AND BUTTONS -->
						<div id="intro-section" class="intro-section">



								

							<!-- WELCOM MESSAGE -->
							<?php
								if( !empty($llorix_one_lite_header_title) ){
									echo '<h1 id="intro_section_text_1" class="intro white-text">'.esc_attr($llorix_one_lite_header_title).'</h1>';
								} elseif ( isset( $wp_customize )   ) {
									echo '<h1 id="intro_section_text_1" class="intro white-text llorix_one_lite_only_customizer"></h1>';
								}
							?>


							<?php
								if( !empty($llorix_one_lite_header_subtitle) ){
									echo '<h5 id="intro_section_text_2" class="white-text">'.esc_attr($llorix_one_lite_header_subtitle).'</h5>';
								} elseif ( isset( $wp_customize )   ) {
									echo '<h5 id="intro_section_text_2" class="white-text llorix_one_lite_only_customizer"></h5>';
								}
							?>

							<!-- BUTTON -->
							<?php
								if( !empty($llorix_one_lite_header_button_text) ){
									if( empty($llorix_one_lite_header_button_link) ){
										echo '<button id="inpage_scroll_btn" class="btn btn-primary standard-button inpage-scroll"><span class="screen-reader-text">'.esc_html__('Header button label:','llorix-one-lite').$llorix_one_lite_header_button_text.'</span>'.$llorix_one_lite_header_button_text.'</button>';
									} else {
										if(strpos($llorix_one_lite_header_button_link, '#') === 0) {
											echo '<button id="inpage_scroll_btn" class="btn btn-primary standard-button inpage-scroll" data-anchor="'.$llorix_one_lite_header_button_link.'"><span class="screen-reader-text">'.esc_html__('Header button label:','llorix-one-lite').$llorix_one_lite_header_button_text.'</span>'.$llorix_one_lite_header_button_text.'</button>';
										} else {
											echo '<button id="inpage_scroll_btn" class="btn btn-primary standard-button inpage-scroll" onClick="parent.location=\''.esc_url($llorix_one_lite_header_button_link).'\'"><span class="screen-reader-text">'.esc_html__('Header button label:','llorix-one-lite').$llorix_one_lite_header_button_text.'</span>'.$llorix_one_lite_header_button_text.'</button>';
										}
									}
								} elseif ( isset( $wp_customize )   ) {
									echo '<div id="intro_section_text_3" class="button"><div id="inpage_scroll_btn"><a href="" class="btn btn-primary standard-button inpage-scroll llorix_one_lite_only_customizer"></a></div></div>';
								}
							?>
							<!-- /END BUTTON -->

						</div>
				</div>
			</div>
			</div>
		</div>


<!-- =========================
 SECTION: Titulo
============================== -->
<?php
	global $wp_customize;
	$llorix_one_lite_our_story_image = get_theme_mod('llorix_one_lite_our_story_image', llorix_one_lite_get_file('/images/about-us.png'));
	$llorix_one_lite_our_story_title = get_theme_mod('llorix_one_lite_our_story_title',esc_html__('Our Story','llorix-one-lite'));
	$llorix_one_lite_our_story_text = get_theme_mod('llorix_one_lite_our_story_text',esc_html__('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','llorix-one-lite'));
	$llorix_one_lite_ribbon_title = get_theme_mod('llorix_one_lite_ribbon_title',esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit.','llorix-one-lite'));	
	$llorix_one_lite_our_story_show = get_theme_mod('llorix_one_lite_our_story_show');
	
	/* If section is not disabled */
	if( isset($llorix_one_lite_our_story_show) && $llorix_one_lite_our_story_show != 1 ) {
		
		if( !empty($llorix_one_lite_our_story_image) || !empty($llorix_one_lite_our_story_title) || !empty($llorix_one_lite_our_story_text) ) {
	?>
			<section class="brief text-center brief-design-one brief-center" id="story" role="region" aria-label="<?php esc_html_e('About','llorix-one-lite') ?>">
				<div class="section-overlay-layer">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
							<?php
								if( !empty($llorix_one_lite_ribbon_title) ){
									echo '<h2 class="black-text strong">'.get_bloginfo( 'name' ).'</h2>';
								} elseif ( isset( $wp_customize )   ) {
									echo '<h2 class="black-text strong llorix_one_lite_only_customizer">'.get_bloginfo( 'name' ).'</h2>';
								}
							?>
							</div>
						</div>
					</div>
				</div>
			</section><!-- .brief-design-one -->
	<?php
		}
	/* If section is disabled, but we are in Customize, display section with class llorix_one_lite_only_customizer */	
	} 
	?>

<!-- =========================
 SECTION: RIBBON   
============================== -->
<?php
	global $wp_customize;
	$ribbon_background = get_theme_mod('llorix_one_lite_ribbon_background', llorix_one_lite_get_file('/images/background-images/parallax-img/parallax-img1.jpg'));
	$llorix_one_lite_ribbon_title = get_theme_mod('llorix_one_lite_ribbon_title',esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit.','llorix-one-lite'));
	$llorix_one_lite_button_text = get_theme_mod('llorix_one_lite_button_text',esc_html__('GET STARTED','llorix-one-lite'));
	$llorix_one_lite_button_link = get_theme_mod('llorix_one_lite_button_link','#');
	
	$llorix_one_lite_ribbon_show = get_theme_mod('llorix_one_lite_ribbon_show');
	
	/* If section is not disabled */
	if( isset($llorix_one_lite_ribbon_show) && $llorix_one_lite_ribbon_show != 1 ) {

		if( !empty($llorix_one_lite_ribbon_title) || !empty($llorix_one_lite_button_text) ) {
			
			if(!empty($ribbon_background)){
				echo '<section class="call-to-action ribbon-wrap" id="ribbon" style="background-image:url('.$ribbon_background.');" role="region" aria-label="'.esc_html__('Ribbon','llorix-one-lite').'">';
			} else {
				echo '<section class="call-to-action ribbon-wrap" id="ribbon" role="region" aria-label="'.esc_html__('Ribbon','llorix-one-lite').'">';
			}
		
	?>
		<div class="overlay-layer-wrap">
			<div class="container overlay-layer" id="llorix_one_lite_header">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">

						</div>
					</div>
				</div>
			</div>
		
	<?php
		}
	/* If section is disabled, but we are in Customize, display section with class llorix_one_lite_only_customizer */
	} elseif( isset( $wp_customize ) ) {
		if(!empty($ribbon_background)){
			echo '<section class="call-to-action ribbon-wrap llorix_one_lite_only_customizer" id="ribbon" style="background-image:url('.$ribbon_background.');" role="region" aria-label="'.esc_html__('Ribbon','llorix-one-lite').'">';
		} else {
			echo '<section class="call-to-action ribbon-wrap llorix_one_lite_only_customizer" id="ribbon" role="region" aria-label="'.esc_html__('Ribbon','llorix-one-lite').'">';
		}
?>
			<div class="section-overlay-layer">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">

							<?php
								if( !empty($llorix_one_lite_ribbon_title) ){
									echo '<h2 class="white-text strong">'.esc_attr($llorix_one_lite_ribbon_title).'</h2>';
								} elseif ( isset( $wp_customize )   ) {
									echo '<h2 class="white-text strong llorix_one_lite_only_customizer"></h2>';
								}

								if( !empty($llorix_one_lite_button_text) ){
									if( empty($llorix_one_lite_button_link) ){
										echo '<button class="btn btn-primary standard-button llorix_one_lite_only_customizer" type="button" data-toggle="modal" data-target="#stamp-modal"><span class="screen-reader-text">'.esc_html__('Ribbon button label:','llorix-one-lite').$llorix_one_lite_button_text.'</span>'.$llorix_one_lite_button_text.'</button>';
									} else {
										echo '<button onclick="window.location=\''.esc_url($llorix_one_lite_button_link).'\'" class="btn btn-primary standard-button" type="button" data-toggle="modal" data-target="#stamp-modal"><span class="screen-reader-text">'.esc_html__('Ribbon button label:','llorix-one-lite').$llorix_one_lite_button_text.'</span>'.esc_attr($llorix_one_lite_button_text).'</button>';
									}
								} elseif ( isset( $wp_customize ) ) {
									echo '<button class="btn btn-primary standard-button llorix_one_lite_only_customizer" type="button" data-toggle="modal" data-target="#stamp-modal"></button>';
								}
							?>

						</div>
					</div>
				</div>
			</div>
		</section>
	<?php
	}
?>


<!-- =========================
 SECTION: LATEST NEWS   
============================== -->
<?php
	global $wp_customize;
	$llorix_one_lite_latest_news_show = get_theme_mod('llorix_one_lite_latest_news_show');
	
	$llorix_one_lite_number_of_posts = get_option('posts_per_page');
	$args = array( 'post_type' => 'book-review', 'posts_per_page' => $llorix_one_lite_number_of_posts, 'order' => 'DESC','ignore_sticky_posts' => true );	
	
	/* If section is not disabled */
	if( isset($llorix_one_lite_latest_news_show) && $llorix_one_lite_latest_news_show != 1 ) {

		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) {
			$llorix_one_lite_latest_news_title = get_theme_mod('llorix_one_lite_latest_news_title',esc_html__('Cuentos','llorix-one-lite'));
			if($llorix_one_lite_number_of_posts > 0) {
			?>
				<section class="brief timeline" id="latestnews" role="region" aria-label="<?php esc_html_e('Utimos cuentos','llorix-one-lite'); ?>">
					<div class="section-overlay-layer">
						<div class="container">
							<div class="content-area col-md-8">

								<!-- TIMELINE HEADING / TEXT  -->
								<?php
									if(!empty($llorix_one_lite_latest_news_title)){
										echo '<div class="col-md-12 timeline-text text-left"><h2 class="text-left dark-text">'.esc_attr($llorix_one_lite_latest_news_title).'</h2><div class="colored-line-left"></div></div>';
									} elseif ( isset( $wp_customize )   ) {
										echo '<div class="col-md-12 timeline-text text-left llorix_one_lite_only_customizer"><h2 class="text-left dark-text "></h2><div class="colored-line-left "></div></div>';
									}
								?>

								<div class="llorix-one-lite-slider-whole-wrap">
									<div class="controls-wrap">
										<button class="control_next fa fa-angle-up"><span class="screen-reader-text"><?php esc_attr_e('Post slider navigation: Down','llorix-one-lite')?></span></button>
										<button class="control_prev fade-btn fa fa-angle-down"><span class="screen-reader-text"><?php esc_attr_e('Post slider navigation: Up','llorix-one-lite')?></span></button>
									</div>
									<!-- TIMLEINE SCROLLER -->
									<div itemscope itemtype="http://schema.org/Blog" id="llorix_one_slider" class="timeline-section">
										<ul>

											<?php

												$i_latest_posts= 0;

												while (  $the_query->have_posts() ) :  $the_query->the_post();

													$i_latest_posts++;

													if ( !wp_is_mobile() ){
														if($i_latest_posts % 14 == 1){
															echo '<li>';
														}
													} else  {
														echo '<li>';
													}
											?>

													<div itemscope itemprop="blogPosts" itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" class="timeline-box-wrap" title="<?php printf( esc_html__( 'Cuentos: %s', 'llorix-one-lite' ), get_the_title() ) ?>">
														<div datetime="<?php the_time( 'Y-m-d\TH:i:sP' ); ?>" title="<?php the_time( _x( 'l, F j, Y, g:i a', 'post time format', 'llorix-one-lite' ) ); ?>" class="entry-published date small-text strong">
														<?php echo get_the_date('M, j'); ?>
														</div>
														<div itemscope itemprop="image" class="icon-container white-text">
															<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
																<?php 

																	if ( has_post_thumbnail() ) :
																		the_post_thumbnail('llorix-one-lite-post-thumbnail-latest-news');
																	else: ?>
																		<img src="<?php echo llorix_one_lite_get_file('/images/no-thumbnail-latest-news.jpg'); ?>" width="150" height="150" alt="<?php the_title(); ?>">
																<?php 
																	endif; 
																?>
															</a>
														</div>
														<div class="info">
															<header class="entry-header">
																<h1 itemprop="headline" class="entry-title">
																	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
																</h1>
																<div class="entry-meta">
																	<span class="entry-date">
																		<a href="<?php echo esc_url( get_day_link(get_the_date('Y'), get_the_date('m'), get_the_date('d')) ) ?>" rel="bookmark">
																			<time itemprop="datePublished" datetime="<?php the_time( 'Y-m-d\TH:i:sP' ); ?>" title="<?php the_time( _x( 'l, F j, Y, g:i a', 'post time format', 'llorix-one-lite' ) ); ?>" class="entry-date entry-published updated"><?php echo the_time( get_option('date_format') ); ?></time>
																		</a>
																	</span>
																	<span> <?php esc_html_e('by','llorix-one-lite');?> </span>
																	<span itemscope itemprop="author" itemtype="http://schema.org/Person" class="author-link">
																		<span  itemprop="name" class="entry-author author vcard">
																			<a itemprop="url" class="url fn n" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )); ?>" rel="author"><?php the_author(); ?> </a>
																		</span>
																	</span>
																</div><!-- .entry-meta -->
															</header>
															<div itemprop="description" class="entry-content entry-summary">
																<?php the_excerpt(); ?>
																<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more"><?php printf( esc_html__( 'Leer Mas %s', 'llorix-one-lite' ), '<span class="screen-reader-text">  '.get_the_title().'</span>' ); ?></a>
															</div>
														</div>
													</div>

												<?php
												if ( !wp_is_mobile() ){
													if($i_latest_posts % 4 == 0){
														echo '</li>';
													}
												} else {
													echo '</li>';
												}

											endwhile;
											wp_reset_postdata(); 
											?>
										</ul>
									</div>
								</div><!-- .llorix-one-lite-slider-whole-wrap -->
							</div>
									<?php
			}
		} 
	/* If section is disabled, but we are in Customize, display section with class llorix_one_lite_only_customizer */
	} 
?>

<div itemscope="" itemtype="http://schema.org/WPSideBar" role="complementary" aria-label="Main sidebar" id="sidebar-secondary" class="col-md-4 widget-area">
							<?php dynamic_sidebar( 'custom' ); ?>
</div>
						</div>
					</div>
				</section>

<!-- =========================
 SECTION: ABOUT
============================== -->
<?php
	global $wp_customize;
	$llorix_one_lite_our_story_image = get_theme_mod('llorix_one_lite_our_story_image', llorix_one_lite_get_file('/images/about-us.png'));
	$llorix_one_lite_our_story_title = get_theme_mod('llorix_one_lite_our_story_title',esc_html__('Our Story','llorix-one-lite'));
	$llorix_one_lite_our_story_text = get_theme_mod('llorix_one_lite_our_story_text',esc_html__('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','llorix-one-lite'));
	
	$llorix_one_lite_our_story_show = get_theme_mod('llorix_one_lite_our_story_show');
	
	/* If section is not disabled */
	if( isset($llorix_one_lite_our_story_show) && $llorix_one_lite_our_story_show != 1 ) {
		
		if( !empty($llorix_one_lite_our_story_image) || !empty($llorix_one_lite_our_story_title) || !empty($llorix_one_lite_our_story_text) ) {
	?>
			<section class="brief text-left brief-design-one brief-left" id="story" role="region" aria-label="<?php esc_html_e('About','llorix-one-lite') ?>">
				<div class="section-overlay-layer">
					<div class="container">
						<div class="row">
							<!-- BRIEF IMAGE -->
							<?php
								if( !empty($llorix_one_lite_our_story_image) ){
									if( !empty($llorix_one_lite_our_story_title) ){
										echo '<div class="col-md-6 brief-content-two"><div class="brief-image-right"><img src="'.esc_url($llorix_one_lite_our_story_image).'" alt="'.esc_attr($llorix_one_lite_our_story_title).'"></div></div>';
									} else {
										echo '<div class="col-md-6 brief-content-two"><div class="brief-image-right"><img src="'.esc_url($llorix_one_lite_our_story_image).'" alt="'.esc_html__('About','llorix-one-lite').'"></div></div>';
									}
								} elseif ( isset( $wp_customize ) ) {
									echo '<div class="col-md-6 brief-content-two llorix_one_lite_only_customizer"><img src="" alt=""><div class="brief-image-right"></div></div>';
								}
							?>

							<!-- BRIEF HEADING -->
							<div class="col-md-6 content-section brief-content-one">
								<?php
									if( !empty($llorix_one_lite_our_story_title) ){
										echo '<h2 class="text-left dark-text">'.esc_attr($llorix_one_lite_our_story_title).'</h2><div class="colored-line-left"></div>';
									} elseif ( isset( $wp_customize )   ) {
										echo '<h2 class="text-left dark-text llorix_one_lite_only_customizer"></h2><div class="colored-line-left llorix_one_lite_only_customizer"></div>';
									}

									if( !empty($llorix_one_lite_our_story_text) ){
										echo '<div class="brief-content-text">'.$llorix_one_lite_our_story_text.'</div>';
									} elseif ( isset( $wp_customize )   ) {
										echo '<div class="brief-content-text llorix_one_lite_only_customizer"></div>';
									}
								?>
							</div><!-- .brief-content-one-->
						</div>
					</div>
				</div>
			</section><!-- .brief-design-one -->
	<?php
		}
	/* If section is disabled, but we are in Customize, display section with class llorix_one_lite_only_customizer */	
	} elseif( isset( $wp_customize ) ) {
	?>
			<section class="brief text-left brief-design-one brief-left llorix_one_lite_only_customizer" id="story" role="region" aria-label="<?php esc_html_e('About','llorix-one-lite') ?>">
				<div class="section-overlay-layer">
					<div class="container">
						<div class="row">
							<!-- BRIEF IMAGE -->
							<?php
								if( !empty($llorix_one_lite_our_story_image) ){
									if( !empty($llorix_one_lite_our_story_title) ){
										echo '<div class="col-md-6 brief-content-two"><div class="brief-image-right"><img src="'.esc_url($llorix_one_lite_our_story_image).'" alt="'.esc_attr($llorix_one_lite_our_story_title).'"></div></div>';
									} else {
										echo '<div class="col-md-6 brief-content-two"><div class="brief-image-right"><img src="'.esc_url($llorix_one_lite_our_story_image).'" alt="'.esc_html__('About','llorix-one-lite').'"></div></div>';
									}
								} elseif ( isset( $wp_customize ) ) {
									echo '<div class="col-md-6 brief-content-two llorix_one_lite_only_customizer"><img src="" alt=""><div class="brief-image-right"></div></div>';
								}
							?>

							<!-- BRIEF HEADING -->
							<div class="col-md-6 content-section brief-content-one">
								<?php
									if( !empty($llorix_one_lite_our_story_title) ){
										echo '<h2 class="text-left dark-text">'.esc_attr($llorix_one_lite_our_story_title).'</h2><div class="colored-line-left"></div>';
									} elseif ( isset( $wp_customize ) ) {
										echo '<h2 class="text-left dark-text llorix_one_lite_only_customizer"></h2><div class="colored-line-left llorix_one_lite_only_customizer"></div>';
									}

									if( !empty($llorix_one_lite_our_story_text) ){
										echo '<div class="brief-content-text">'.$llorix_one_lite_our_story_text.'</div>';
									} elseif( isset( $wp_customize ) ) {
										echo '<div class="brief-content-text llorix_one_lite_only_customizer"></div>';
									}
								?>
							</div><!-- .brief-content-one-->
						</div>
					</div>
				</div>
			</section><!-- .brief-design-one -->
<?php
		}
?>